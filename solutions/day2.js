console.log('Day 2')

const fs = require('fs')

// opponent
// A - Rock
// B - Paper
// C - Scissors

// you
// X - Rock (1)
// Y - Paper (2)
// Z - scissors (3)

const winMap = {
    X: 'C',
    Y: 'A',
    Z: 'B',
}

const drawMap = {
    X: 'A',
    Y: 'B',
    Z: 'C',
}

const scoreMap = {
    X: 1,
    Y: 2,
    Z: 3,
}

function fightScore(opponent, you) {
    const draw = opponent === drawMap[you] ? 3 : 0
    const win = opponent === winMap[you] ? 6 : 0
    return scoreMap[you] + draw + win
}

// result
// X - lose
// Y - draw
// Z - win
function getYourOption(opponent, result) {
    const options = Object.values(drawMap) // ['A', 'B', 'C']
    const opponentId = options.findIndex(option => option === opponent) // A -> 0
    const shift = result === 'Y' ? 0 : result === 'Z' ? 1 : -1 // Y -> 0
    let yourId = opponentId + shift // 0
    yourId = yourId === options.length ? 0 : yourId // 3 -> 0
    yourId = yourId < 0 ? options.length - 1 : yourId // -1 -> 2
    return Object.keys(drawMap)[yourId] // ['X', 'Y', 'Z'][0] -> 'X'
}

function fightScore2(opponent, result) {
    let sum = result === 'Y' ? 3 : 0
    sum+= result === 'Z' ? 6 : 0
    const yourOption = getYourOption(opponent, result)
    return scoreMap[yourOption] + sum
}

try {
    const data = fs.readFileSync('../input2.txt', 'utf8')
    const result = data.split('\n').map(record => {
        const [opponent, you] = record.split(' ')
        return fightScore2(opponent, you)
    }).reduce((acc, curr) => acc + curr, 0)
    console.log(result)
} catch (err) {
    console.error(err)
}
