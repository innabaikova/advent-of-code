console.log('Day 4')

const fs = require('fs')
// 2-4,6-8
// 2-3,4-5
// 5-7,7-9 fEnd === sStart
// 2-8,3-7 sStart > fStart && sStart < fEnd
// 6-6,4-6 sStart < fEnd
// 2-6,4-8 sStart < fEnd
// 3-7,2-8 

//12-13,10-11
//11-15,10-12 sEnd >=fStart && sStart <= fStart

function isFullyContains(record) {
    const [firs, second] = record.split(',')
    const [fStart, fEnd] = firs.split('-').map(i => +i)
    const [sStart, sEnd] = second.split('-').map(i => +i)
    const isFirstContainsSecond = fStart <= sStart && fEnd >= sEnd
    const isSecondContainsFirst = sStart <= fStart && sEnd >= fEnd
    return isFirstContainsSecond || isSecondContainsFirst
}

function isOverlaps(record) {
    const [firs, second] = record.split(',')
    const [fStart, fEnd] = firs.split('-').map(i => +i)
    const [sStart, sEnd] = second.split('-').map(i => +i)
    return sStart <= fEnd && sStart >= fStart || sEnd >=fStart && sStart <= fStart
}

try {
    const data = fs.readFileSync('../input4.txt', 'utf8')
    const result = data
        .split('\n')
        .filter(r => isOverlaps(r))
    console.log(result.length)
} catch (err) {
    console.error(err)
}
