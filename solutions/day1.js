console.log("Day 1")

const fs = require('fs');

try {
  const data = fs.readFileSync('../input.txt', 'utf8');
  const caloriesPerElf = data.split('\n\n').map(record => {
    const calories = record.split('\n')
    return calories.reduce((acc, curr) => acc + +curr, 0)
  })
  const sorted = caloriesPerElf.sort((a,b) => b-a)
  console.log(sorted[0]);
  const topThreeSum = sorted.slice(0,3).reduce((a,b) => a+b)
  console.log(topThreeSum)
} catch (err) {
  console.error(err);
}