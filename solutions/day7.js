console.log('Day 7')

const fs = require('fs')

function calculate(commands) {
    let result = {}
    let path = []
    let currDir
    for (let command of commands) {
        if (command.startsWith('$ cd')) {
            const [_, folder] = command.split('$ cd ')
            if (folder !== '..') {
                if (currDir) path.push(currDir)
                currDir = path.slice(1).concat(folder).join('/')
            } else {
                currDir = path.pop()
            }
        } else if (command !== '$ ls') {
            const [val] = command.split(' ')
            if (val !== 'dir') {
                result[currDir] = result[currDir] || 0
                const bytes = +val
                result[currDir] += bytes
                for (let dir of path) {
                    if (!result[dir]) result[dir] = 0
                    result[dir] += bytes
                }
            }
        }
    }
    return result
}

try {
    const data = fs.readFileSync('../input7.txt', 'utf8')
    const commands = data.split('\n')
    const folderSums = calculate(commands)
    const sizes = Object.values(folderSums)
    const result = sizes.filter(s => s <= 100000).reduce((acc, curr) => acc+curr,0)
    console.log(result)

    let spaceNeeded = 30000000 - (70000000 - folderSums["/"])
    const deleted = sizes.filter(s => s >= spaceNeeded).sort((a,b) => a-b)[0]
    console.log(deleted)
} catch (err) {
    console.error(err)
}
