console.log("Day 5")

const fs = require('fs')

function getStacksMap(stacks) {
    const stacksRows = stacks.split('\n')
    const cols = stacksRows[stacksRows.length-1].split('')
    const map = {}
    const indexes = []

    for (let i=0; i<cols.length; i++) {
        if (/\d/.test(cols[i])) {
            indexes.push(i)
            map[cols[i]] = []
        }
    }

    for (let r=0; r<stacksRows.length-1; r++) {
        const row = stacksRows[r].split('')
        let colId = 0
        for (let i=0; i<indexes.length; i++) {
            colId++
            const idx = indexes[i]
            let s = row[idx]
            if (/\w/.test(s)) map[colId].push(s)
        }
    }
    return map
}

function move(record, map) {
    const [count, fromCol, toCol] = record.match(/\d+/g)
    for (let i=0; i<count; i++) {
        const item = map[fromCol].shift()
        map[toCol].unshift(item)
    }
}

function move2(record, map) {
    const [count, fromCol, toCol] = record.match(/\d+/g)
    const items = map[fromCol].splice(0, count)
    map[toCol] = [...items, ...map[toCol]]
}



try {
    const data = fs.readFileSync('../input5.txt', 'utf8')
    const [stacks, operations] = data.split('\n\n')
    const stacksMap = getStacksMap(stacks)

    for (let operation of operations.split('\n')) {
        move2(operation, stacksMap)
    }

    const result = Object.values(stacksMap).reduce((acc, curr) => acc + curr[0], '')
    console.log(stacks)
    console.log(result)
} catch (err) {
    console.error(err)
}