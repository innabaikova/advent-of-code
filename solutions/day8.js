console.log('Day 8')

const fs = require('fs')

function countVisibleTrees(rows) {
    let count = 0
    for (let i=1; i<rows.length-1; i++) {
        for (let j=1; j<rows[0].length-1; j++) {
            const curr = rows[i][j]
            const trees = rows[i].split('')

            const leftSide = trees.slice(0, j)
            const isVisibleLeft = leftSide.every(tree => tree < curr)

            const rightSide = trees.slice(j+1,trees.length)
            const isVisibleRight = rightSide.every(tree => tree < curr)
            
            const topSide = rows.slice(0, i).reduce((acc, row) => [...acc, row[j]], [])
            const isVisibleTop = topSide.every(tree => tree < curr)

            const bottomSide = rows.slice(i+1, rows.length).reduce((acc, row) => [...acc, row[j]], [])
            const isVisibleBottom = bottomSide.every(tree => tree < curr)

            if (isVisibleLeft || isVisibleRight || isVisibleTop || isVisibleBottom) {
                count++
            }
        }
    }
    return count
}

function foundMaxScenicScore(rows) {
    let maxScenicScore = 1
    for (let i=1; i<rows.length-1; i++) {
        for (let j=1; j<rows[0].length-1; j++) {
            const curr = rows[i][j]
            const trees = rows[i].split('')

            const leftSide = trees.slice(0, j)
            let leftScore = 0
            for (let k=leftSide.length-1; k>=0; k--) {
                leftScore++
                if (leftSide[k] >= curr) break;
            }

            const rightSide = trees.slice(j+1,trees.length)
            let rightScore = 0
            for (let k=0; k<rightSide.length; k++) {
                rightScore++
                if (rightSide[k] >= curr) break;
            }
            
            const topSide = rows.slice(0, i).reduce((acc, row) => [...acc, row[j]], [])
            let topScore = 0
            for (let k=topSide.length-1; k>=0; k--) {
                topScore++
                if (topSide[k] >= curr) break;
            }

            const bottomSide = rows.slice(i+1, rows.length).reduce((acc, row) => [...acc, row[j]], [])
            let bottomScore = 0
            for (let k=0; k<bottomSide.length; k++) {
                bottomScore++
                if (bottomSide[k] >= curr) break;
            }

            const scenicScore = leftScore * rightScore * bottomScore * topScore
            maxScenicScore = maxScenicScore < scenicScore ? scenicScore : maxScenicScore
        }
    }
    return maxScenicScore
}

try {
    const data = fs.readFileSync('../input8.txt', 'utf8')
    const rows = data.split('\n')
    const edgeTrees = 2 * (rows.length - 2) + rows[0].length * 2
    const visible = countVisibleTrees(rows)
    console.log(edgeTrees)
    console.log(visible)
    console.log(edgeTrees + visible)

    const score = foundMaxScenicScore(rows)
    console.log(score)
} catch (err) {
    console.error(err)
}