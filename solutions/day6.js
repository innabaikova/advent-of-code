console.log("Day 6")

const fs = require('fs')

function getMarker(str) {
    let marker = str.slice(0,14)
    for (let i=4; i<str.length; i++) {
        const set = new Set(marker)
        const isUnique = set.size === 14
        if (isUnique) {
            return i;
        } else {
            marker = marker.substr(1) + str[i]
        }
    }
}

console.log(getMarker('zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw'))

try {
    const data = fs.readFileSync('../input6.txt', 'utf8')
    const result = getMarker(data)
    console.log(result)
} catch (err) {
    console.error(err)
}