console.log('Day 3')

const fs = require('fs')

const firstPart = Array.from(Array(26)).map((e, i) => i + 97)
let dictionary = firstPart.reduce((acc, curr) => {
    acc[String.fromCharCode(curr)] = curr - 96
    return acc
}, {})

const secondPart = Array.from(Array(26)).map((e, i) => i + 65)
dictionary = secondPart.reduce((acc, curr) => {
    acc[String.fromCharCode(curr)] = curr - 38
    return acc
}, dictionary)

function findItem(items) {
    const first = items.slice(0, items.length / 2)
    const second = items.slice(items.length / 2)
    const mistake = [...first].find(item => second.includes(item))
    return dictionary[mistake]
}

function findBadges(records) {
    let sum = 0
    for (let i=0; i< records.length; i=i+3) {
        const first = records[i]
        const second = records[i+1]
        const third = records[i+2]
        const badge = [...first].find(item => second.includes(item) && third.includes(item))
        sum += dictionary[badge]
    }
    return sum
}

const test = ['vJrwpWtwJgWrhcsFMMfFFhFp', 'jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL', 'PmmdzqPrVvPwwTWBwg']
findBadges(test)

try {
    const data = fs.readFileSync('../input3.txt', 'utf8')
    // const result = data
    //     .split('\n')
    //     .map(record => findItem(record))
    //     .reduce((acc, curr) => acc + curr, 0)
    const result = findBadges(data.split('\n'))
    console.log(result)
} catch (err) {
    console.error(err)
}
