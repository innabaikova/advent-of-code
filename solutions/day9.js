console.log('Day 9')

const fs = require('fs')

function calculate(commands) {
    const fieldLength = 5
    const fieldHeight = 4
    let H = [0,4]
    let T = [0,4]
    const positions = new Set()

    for (let command of commands) {
        const [dir, steps] = command.split(" ")
        if (dir === 'R') {
            for (let i=0; i<steps; i++) {
                const idx = H[0] + 1
                T = idx - T[0] === 2 ? [...H] : T
                H[0] = idx > fieldLength ? fieldLength : idx
                positions.add(T.toString())
            }
        }
        if (dir === 'U') {
            for (let i=0; i<steps; i++) {
                const idx = H[1] - 1
                T = T[1] - idx === 2 ? [...H] : T
                H[1] = idx > 0 ? idx : 0
                positions.add(T.toString())
            }
        }
        if (dir === 'L') {
            for (let i=0; i<steps; i++) {
                const idx = H[0] - 1
                T = T[0] - idx === 2 ? [...H] : T
                H[0] = idx > 0 ? idx : 0
                positions.add(T.toString())
            }
        }
        if (dir === 'D') {
            for (let i=0; i<steps; i++) {
                const idx = H[1] + 1
                T = idx - T[1] === 2 ? [...H] : T
                H[1] = idx > fieldHeight ? fieldHeight : idx
                positions.add(T.toString())
            }
        }
        console.log(positions)
    }
    return positions.size
}

try {
    const data = fs.readFileSync('../input9.txt', 'utf8')
    const commands = data.split('\n')
    console.log(calculate(commands))
} catch (err) {
    console.error(err)
}